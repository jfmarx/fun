# Fun

Just for fun; a chance to showcase the type of work I do as a data science professional, but with public datasets. 

## Direct Links

* _Must Love Dogs_ -- exploring and k-means clustering dogs via several public datasets; https://jfmarx.gitlab.io/fun/must_love_dogs.nb.html/
* _Text Mining + Sentiment Analysis with Twitter_ -- what can we learn from a week's worth of tweets mentioning Nordstrom?  | https://jfmarx.gitlab.io/fun/twitter_text_mining_notebook.nb.html/
* _The ColoR Wheel_ -- your color palette matters / R is here to help; presenting some essential tools and tricks of the trade | https://gitlab.com/jfmarx/fun/blob/master/colors_in_r.pdf

## Also
* <a href = "https://drive.google.com/file/d/1_3FIC5FWBrHheQ7Gz1nlq4TDEwG1VYIF/view?usp=sharing" target="_blank">Resume</a>
