# Manipulating DataFrames with Pandas

## Indexing

### Hierarchical Indexing
- Solution: use tuples
- We can create an ordered way to identify unique keys when a single column won't do. 

```python
stocks = stocks.set_index(['symbol', 'date'])
# Because there is a unique row for every symbol + date
# reorder the index
stocks = stocks.sort_index()

# Easier to index and slice
stocks.loc[('JWN', '2020-01-01'), 'closing_price']

# Slice both indexes (this shows all columns for all symbols for the dates selected ordered by symbol, date)
stocks.loc[(slice(None), slice('2020-01-01', '2020-02-01')), :]

# New example where index is 'state', 'month':
# Look up data for NY in month 1 in sales: NY_month1
NY_month1 = sales.loc[('NY', 1), :]

# Look up data for CA and TX in month 2: CA_TX_month2
CA_TX_month2 = sales.loc[(['CA', 'TX'], 2), :]

# Look up data for all states in month 2: all_month2
all_month2 = sales.loc[(slice(None), 2), :]
```

## Pivoting DataFrames
Ways to reshape data; similar to `reshape`, and `spread` in `tidyr`. 

### Pivoting single and multiple columns
```python
trials.pivot(index='treatment',
            columns='gender',
            values='response')
#gives new df with treatment type as index, columns as gender and values are reponse; this would be akin to spread(gender, response)

#unlike spread - you can have multiple subsets of values - this is called pivoting multiple columns. it will group each value set stratefied by gender. 
trials.pivot(index='treatment', columns='gender')
```

### Stacking + Unstacking DataFrames
- Unstack: make long df wide; if you have a multilevel index - you can unstack one of the indexes: 
`trials.unstack(level='gender')`
- Stack: make wide df long; specifiy hierarchial columns to move to the index. 
`trials_by_gender.stack(level = 'gender')`
- Switch index levels: `swapped = stacked.swaplevel(0, 1)` then resort index so rows will make sense: `swapped.sort_index()`. 

### Melting DataFrames
- Restore a pivoted dataframe to its original form or change from wide to long. 
- `pd.melt(df, id_vars=['treatment'])` or 
`pd.melt(df, id_vars=['treatment'], var_name='gender', value_name='response')`

### Pivot Tables 
- Use when there are not unique rows of within feature columns. 
- Uses a reduction; defaults to average, but we can change using the `aggfunc` argument within the `.pivot_table` method. 

```python
more_trials.pivot_table(index='treatment', columns='gender', values='response', aggfunc='count') # count instead of average (frequency table)
```

## Categoricals and groupby
- Groupby example: `sales.groupby('weekday').count()`
- Groupby example but limited to certain columns: `sales.groupby('weekday').[['bread', 'butter']].sum()` 
- Groupby with multi-level index: `sales.groupby(['city', 'weekday']).mean()`
- See all unique values in a column: `sales['weekday'].unique()`
- Best to convert categorical features to categories when possible (for perfomance + memory): `sales['weekday'] = sales['weekday'].astype('category')`

#### Creating and reordering categories
```python
# Redefine 'Medal' as an ordered categorical
medals.Medal = pd.Categorical(values = medals.Medal, categories=['Bronze', 'Silver', 'Gold'], ordered=True)

# Create the DataFrame: usa
usa = medals[medals.NOC == 'USA']

# Group usa by 'Edition', 'Medal', and 'Athlete'
usa_medals_by_year = usa.groupby(['Edition', 'Medal'])['Athlete'].count()

# Reshape usa_medals_by_year by unstacking
usa_medals_by_year = usa_medals_by_year.unstack(level='Medal')

# Create an area plot of usa_medals_by_year
usa_medals_by_year.plot.area()
plt.show()
```

### Group one dataframe by the series of another
```python
# Read life_fname into a DataFrame: life
life = pd.read_csv(life_fname, index_col='Country')

# Read regions_fname into a DataFrame: regions
regions = pd.read_csv(regions_fname, index_col='Country')

# Group life by regions['region']: life_by_region
life_by_region = life.groupby(regions['region'])

# Print the mean over the '2010' column of life_by_region
print(life_by_region['2010'].mean())
```
### Groupby and aggregations  

```python 
#multiple aggregations: max and sum
sales.groupby('city')[['bread', 'butter']].agg(['max', 'sum'])

#create your own aggregation method - each series is the vector of the column that is being aggregated. 
def data_range(series): 
    return series.max() - series.min()

sales.groupby('city')[['bread', 'butter']].agg(data_range)
```

#### Using a dictionary for aggregation titles
```python
# Read the CSV file into a DataFrame and sort the index: gapminder
gapminder = pd.read_csv('gapminder.csv', index_col=['Year','region','Country']).sort_index()

# Group gapminder by 'Year' and 'region': by_year_region
by_year_region = gapminder.groupby(['Year', 'region'])

# Define the function to compute spread: spread
def spread(series):
    return series.max() - series.min()

# Create the dictionary: aggregator
aggregator = {'population':'sum', 'child_mortality':'mean', 'gdp':spread}

# Aggregate by_year_region using the dictionary: aggregated
aggregated = by_year_region.agg(aggregator)

# Print the last 6 entries of aggregated 
print(aggregated.tail(6))
```

RANDOM: get day of week from date-time: `sales.index.strftime('%a')`

### Groupby and transformations
- Transformations are like `mutate`
- Don't need to aggregate or transform? Use `.apply()`
```python
# Import zscore
from scipy.stats import zscore

# Group gapminder_2010: standardized
standardized = gapminder_2010.groupby('region')['life', 'fertility'].transform(zscore)

# Construct a Boolean Series to identify outliers: outliers
outliers = (standardized['life'] < -3) | (standardized['fertility'] > 3)

# Filter gapminder_2010 by the outliers: gm_outliers
gm_outliers = gapminder_2010.loc[outliers]

# Print gm_outliers
print(gm_outliers)
```

### Fill missing data (imputation) by group
```python
# Create a groupby object: by_sex_class
by_sex_class = titanic.groupby(['sex', 'pclass'])

# Write a function that imputes median
def impute_median(series):
    return series.fillna(series.median())

# Impute age and assign to titanic['age']
titanic['age'] = by_sex_class['age'].transform(impute_median)

# Print the output of titanic.tail(10)
print(titanic.tail(10))
```

### Using .apply
The `.apply()`method when used on a groupby object performs an arbitrary function on each of the groups. These functions can be aggregations, transformations or more complex workflows. The `.apply()` method will then combine the results in an intelligent way.
```python
def disparity(gr):
    # Compute the spread of gr['gdp']: s
    s = gr['gdp'].max() - gr['gdp'].min()
    # Compute the z-score of gr['gdp'] as (gr['gdp']-gr['gdp'].mean())/gr['gdp'].std(): z
    z = (gr['gdp'] - gr['gdp'].mean())/gr['gdp'].std()
    # Return a DataFrame with the inputs {'z(gdp)':z, 'regional spread(gdp)':s}
    return pd.DataFrame({'z(gdp)':z , 'regional spread(gdp)':s})

# Group gapminder_2010 by 'region': regional
regional = gapminder_2010.groupby('region')

# Apply the disparity function on regional: reg_disp
reg_disp = regional.apply(disparity)

# Print the disparity of 'United States', 'United Kingdom', and 'China'
print(reg_disp.loc[['United States','United Kingdom','China']])
```
### Groupby and Filtering

```python
# Create a groupby object using titanic over the 'sex' column: by_sex
by_sex = titanic.groupby('sex')

def c_deck_survival(gr):

    c_passengers = gr['cabin'].str.startswith('C').fillna(False)

    return gr.loc[c_passengers, 'survived'].mean()

 # Call by_sex.apply with the function c_deck_survival
c_surv_by_sex = by_sex.apply(c_deck_survival)

# Print the survival rates
print(c_surv_by_sex)   
```
Another filtering example using lambdas: 
```python
# Read the CSV file into a DataFrame: sales
sales = pd.read_csv('sales.csv', index_col='Date', parse_dates=True)

# Group sales by 'Company': by_company
by_company = sales.groupby('Company')

# Compute the sum of the 'Units' of by_company: by_com_sum
by_com_sum = by_company['Units'].sum()
print(by_com_sum)

# Filter 'Units' where the sum is > 35: by_com_filt
by_com_filt = by_company.filter(lambda g:g['Units'].sum() > 35)

print(by_com_filt)
```

### Filter with Boolean Indexing
```python
# Create the Boolean Series: sus
sus = (medals.Event_gender == 'W') & (medals.Gender == 'Men')

# Create a DataFrame with the suspicious row: suspect
suspect = medals[sus]

# Print suspect
print(suspect)
```

## idxmax() and idxmin()
- First is row or column label where max value is located 
- Second is where min value is located 
- If labels are columns use `axis = columns`