__Machine Learning in Python__

# Data Pre-processing and Visualization
## Handling missing data
Techniques:
- Omission 
- Imputation: mean, mode, median, etc. 

Handling inapropriately introduces bias. Need to test which impact the variance. 

### Functions: 
- `df.isna().sum()`: number missing
- `df['feature'].mean()`: feature mean
- `.shape`: row, column dimensions
- `df.columns`: column names
- `.fillna(0)`: fill with 0
- `select_dtypes(include = [np.number])`: numeric columns
- `select_dtypes(include = [object])`: string columns

```python
dropNArows = loan_data.dropna(axis=0)
#percentage of rows remaining
print(dropNArows.shape[0]/loan_data.shape[0] * 100)
```
### Simple imputation 
```python
# Import imputer module
from sklearn.impute import SimpleImputer

# Subset numeric features: numeric_cols
numeric_cols = loan_data.select_dtypes(include=[np.number])

# Impute with mean
imp_mean = SimpleImputer(strategy='mean')
loans_imp_mean = imp_mean.fit_transform(numeric_cols)

# Convert returned array to DataFrame
loans_imp_meanDF = pd.DataFrame(loans_imp_mean, columns=numeric_cols.columns)

# Check the DataFrame's info
print(loans_imp_meanDF.info())
```
### Iterative imputation
```python
# Subset numeric features: numeric_cols
numeric_cols = loan_data.select_dtypes(include=[np.number])

# Iteratively impute
imp_iter = IterativeImputer(max_iter=5, sample_posterior=True, random_state=123)
loans_imp_iter = imp_iter.fit_transform(numeric_cols)

# Convert returned array to DataFrame
loans_imp_iterDF = pd.DataFrame(loans_imp_iter, columns=numeric_cols.columns)

# Check the DataFrame's info
print(loans_imp_iterDF.info())
```

## Different distributions and transformations
- Too much bias / variance if training and test distributions are too different. 

#70% of data is for training 
train, test = train_test_split(X, y, test_size =0.3)

### Train/test distributions
```python
import matplotlib.pyplot as plt
import seaborn as sns
# Create `loan_data` subset: loan_data_subset
loan_data_subset = loan_data[['Credit Score','Annual Income','Loan Status']]

# Create train and test sets
trainingSet, testSet = train_test_split(loan_data_subset, test_size=0.2, random_state=123)

# Examine pairplots
plt.figure()
sns.pairplot(trainingSet, hue='Loan Status', palette='RdBu')
plt.show()

plt.figure()
sns.pairplot(testSet, hue='Loan Status', palette='RdBu')
plt.show()

```

#### Log and Square Root Transformations
```python 
# Subset loan_data
cr_yrs = loan_data['Years of Credit History']

# Log transformation
cr_yrs_log = boxcox(cr_yrs, lmbda=0.0)

# Square root transform
cr_yrs_sqrt = boxcox(cr_yrs, lmbda=0.5)

# Histogram and kernel density estimate
plt.figure()
sns.distplot(cr_yrs_sqrt)
plt.show()
```

### Data outliers and scaling
- above or below 1.5*IQR
- `sns.boxplot(x=, y='Loan Status')` boxplot conditioned on target variable
- `sns.distplot()` histogram and kernal density estimate (kde)
- `np.abs()` absolute value
- `stats.zscore `calculated z-score
- `mstats.winsorize(limits=[0.05, 0.05])` floor and ceiling applied to outliers
- `np.where(condition, true, false)` replaced values
- high variance features will be chosen more often than low variance -- making it seem more important 

#### Standardization vs. normalization 
- Standardization: z-score standardiaztion; scales to mean 0 and sd 1
- Normalization: min/max scaling: values between 0 and 1 

##### Visualizing outliers 
```python
# Import modules
import matplotlib.pyplot as plt
import seaborn as sns

# Univariate and multivariate boxplots
fig, ax =plt.subplots(1,2)
sns.boxplot(y=loan_data['Annual Income'], ax=ax[0])
sns.boxplot(x='Loan Status', y='Annual Income', data=loan_data, ax=ax[1])
plt.show()
```
##### Handling outliers 
_Using IQR_
```python
# Print: before dropping
print(numeric_cols.mean())
print(numeric_cols.median())
print(numeric_cols.max())

# Create index of rows to keep
idx = (np.abs(stats.zscore(numeric_cols)) < 3).all(axis=1)

# Concatenate numeric and categoric subsets
ld_out_drop = pd.concat([numeric_cols.loc[idx], categoric_cols.loc[idx]], axis=1)

# Print: after dropping
print(ld_out_drop.mean())
print(ld_out_drop.median())
print(ld_out_drop.max())
```
_Replacing top and bottom 5% (Winsorize)_
```python
# Print: before winsorize
print((loan_data['Monthly Debt']).mean())
print((loan_data['Monthly Debt']).median())
print((loan_data['Monthly Debt']).max())

# Winsorize numeric columns
debt_win = mstats.winsorize(loan_data['Monthly Debt'], limits=[0.05, 0.05])

# Convert to DataFrame, reassign column name
debt_out = pd.DataFrame(debt_win, columns=['Monthly Debt'])

# Print: after winsorize
print(debt_out.mean())
print(debt_out.median())
print(debt_out.max())
```
_Replace certain values with Median_
```python
# Print: before replace with median
print((loan_data['Monthly Debt']).mean())
print((loan_data['Monthly Debt']).median())
print((loan_data['Monthly Debt']).max())

# Find median
median = loan_data.loc[loan_data['Monthly Debt'] < 2120, 'Monthly Debt'].median()
loan_data['Monthly Debt'] = np.where(loan_data['Monthly Debt'] > 2120, median, loan_data['Monthly Debt'])

print((loan_data['Monthly Debt']).mean())
print((loan_data['Monthly Debt']).median())
print((loan_data['Monthly Debt']).max())
```
_Z-scaler standardization_
```python
from sklearn.preprocessing import StandardScaler
# Subset features
numeric_cols = loan_data.select_dtypes(include=[np.number])
categoric_cols = loan_data.select_dtypes(include=[object])

# Instantiate
scaler = StandardScaler()
# If want to use min-max scaling instead, use scaler = MinMaxScaler() (and remember to import it first)

# Fit and transform, convert to DF
numeric_cols_scaled = scaler.fit_transform(numeric_cols)
numeric_cols_scaledDF = pd.DataFrame(numeric_cols_scaled, columns=numeric_cols.columns)

# Concatenate categoric columns to scaled numeric columns
final_DF = pd.concat([numeric_cols_scaledDF, categoric_cols], axis=1)
print(final_DF.head())
```
# Supervised Learning
## Regression: feature selection
- Reduces overfitting
- Improves accuracy
- Increases interpretibility
- Faster computation

### Feature selection methods
- Filter: Rank features based on statistical performance (corr plots)
- Wrapper: Use an ML method to evaluate performance (forward selection, backwards elimination)
- Embedded: Iterative model training to extract features (lasso, ridge, elasticnet)
- Feature importance: tree-based ML models (Randam forest, extra trees)

_Filter example_
```python
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd 
# Create correlation matrix and print it
cor = diabetes.corr()
print(cor)

# Correlation matrix heatmap
plt.figure()
sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
plt.show()

# Correlation with output variable
cor_target = abs(cor["progression"])

# Selecting highly correlated features
best_features = cor_target[cor_target > .5]

print(best_features)
```
_Wrapper example_
```python 
# Import modules
from sklearn.svm import SVR
from sklearn.feature_selection import RFECV

# Instantiate estimator and feature selector
svr_mod = SVR(kernel="linear")
feat_selector = RFECV(svr_mod, cv=5)

# Fit
feat_selector = feat_selector.fit(X, y)

# Print support and ranking
print(feat_selector.support_)
print(feat_selector.ranking_)
print(X.columns)

# Import modules
from sklearn.linear_model import LarsCV

# Drop feature suggested not important in step 2
X = X.drop('sex', axis=1)

# Instantiate
lars_mod = LarsCV(cv=5, normalize=False)

# Fit
feat_selector = lars_mod.fit(X, y)

# Print r-squared score and estimated alpha
print(lars_mod.score(X, y))
print(lars_mod.alpha_)
```
_Feature importance example_
```python
# Import
from sklearn.ensemble import RandomForestRegressor

# Instantiate
rf_mod = RandomForestRegressor(max_depth=2, random_state=123, 
              n_estimators=100, oob_score=True)

# Fit
rf_mod.fit(X, y)

# Print
print(diabetes.columns)
print(rf_mod.feature_importances_)

# Import
from sklearn.ensemble import ExtraTreesRegressor

# Instantiate
xt_mod = ExtraTreesRegressor()

# Fit
xt_mod.fit(X, y)

# Print
print(diabetes.columns)
print(xt_mod.feature_importances_)
```
_Regularization_
- Uses ordinary least squares 
- Ridge: L2 regularization / L2 norm: sum of square coefficients. 
- Lasso: L1 reg / L1 norm: takes absolute value of coefficients instead of squares (like ridge); shrinks less-important features to zero (as lambda increases). 
- ElasticNet: combo of 2 above; sets alpha automatically. 

#### Lasso
```python
# Import modules
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error

# Train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=123, test_size=0.3)

# Instantiate cross-validated lasso, fit
lasso_cv = LassoCV(alphas=None, cv=10, max_iter=10000)
lasso_cv.fit(X_train, y_train)

# Instantiate lasso, fit, predict and print MSE
lasso = Lasso(alpha = lasso_cv.alpha_)
lasso.fit(X_train, y_train)
print(mean_squared_error(y_true=y_test, y_pred=lasso.predict(X_test)))
```

#### Ridge
```python
# Import modules
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.metrics import mean_squared_error

# Train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=123, test_size=0.3)

# Instantiate cross-validated ridge, fit
ridge_cv = RidgeCV(alphas=np.logspace(-6, 6, 13))
ridge_cv.fit(X_train, y_train)

# Instantiate ridge, fit, predict and print MSE
ridge = Ridge(alpha = ridge_cv.alpha_)
ridge.fit(X_train, y_train)
print(mean_squared_error(y_true=y_test, y_pred=ridge.predict(X_test)))
```

### Classification: feature engineering
- Makes your models perform better! 
- Types: indicator variables, interaction features (mathmematical combinations of multiple features), feature representation (grouping categories or taking a week from a timestamp, one-hot encoding)
 
 _Logistic Regression Example_
 ```python
# Create X matrix and y array
X = loan_data.drop("Loan Status", axis=1)
y = loan_data["Loan Status"]

# Train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=123)

# Instantiate
logistic = LogisticRegression()

# Fit
logistic.fit(X_train, y_train)

# Predict and print accuracy
print(accuracy_score(y_true=y_test, y_pred=logistic.predict(X_test)))

# Create dti_ratio variable
monthly_income = loan_data["Annual Income"]/12
loan_data["dti_ratio"] = loan_data["Monthly Debt"]/monthly_income * 100
loan_data = loan_data.drop(["Monthly Debt","Annual Income"], axis=1)

# Replace target variable levels
loan_data["Loan Status"] = loan_data["Loan Status"].replace({'Fully Paid': 0, 
                                            'Charged Off': 1})

# One-hot encode categorical variables
loan_data = pd.get_dummies(data=loan_data)

# Print
print(loan_data.head())

# Create X matrix and y array
X = loans_dti.drop("Loan Status", axis=1)
y = loans_dti["Loan Status"]

# Train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=123)

# Instantiate
logistic_dti = LogisticRegression()

# Fit
logistic_dti.fit(X_train, y_train)

# Predict and print accuracy
print(accuracy_score(y_true=y_test, y_pred=logistic_dti.predict(X_test)))
 ```

 ### Ensemble Learning Techniques
 - Boosting
 - Bagging
 - Model Stacking

#### Bias-Variance Trade-Off
- High Bias = Underfitting (too simple); poor model generalization. Bias reduces when model complexity increases.
- High Variance = Overfitting (too complex); variance increases when model complexity increases. 
- Bias-Variance Trade-Off: since one goes up when the other goes down, need the right balance to produce a successful model. 
- Bagging(Bootstrap aggregation) reduces variance. 
- Boosting reduces bias. 
- Model stacking uses predictions of base classifiers. 
- Ensemble learner with the highest accuracy score wins (three examples below).

_Bagging(Bootstrap example)_
```python
# Instantiate bootstrap aggregation model
bagged_model = BaggingClassifier(n_estimators=50, random_state=123)

# Fit
bagged_model.fit(X_train, y_train)

# Predict
bagged_pred = bagged_model.predict(X_test)

# Print accuracy score
print(accuracy_score(y_test, bagged_pred))
```
_Boosting_
```python
# Boosting model
boosted_model = AdaBoostClassifier(n_estimators=50, random_state=123)

# Fit
boosted_model_fit = boosted_model.fit(X_train, y_train)

# Predict
boosted_pred = boosted_model_fit.predict(X_test)

# Print model accuracy
print(accuracy_score(y_test, boosted_pred))
```
_Boosting with XGBoost_
```python
# Instantiate
xgb = XGBClassifier(random_state=123, learning_rate=0.1, n_estimators=10, max_depth=3)

# Fit
xgb = xgb.fit(X_train, y_train)

# Predict
xgb_pred = xgb.predict(X_test)

# Print accuracy score
print('Final prediction score: [%.8f]' % accuracy_score(y_test, xgb_pred))
```
# Unsupervised Learning 
## Dimensionality reduction: feature extraction




